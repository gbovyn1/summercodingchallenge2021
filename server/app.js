const express = require('express')
const app = express()
const bodyParser = require('body-parser');
const players = require('../players')

const cors = require('cors')
const port = 5001

app.use(cors())

app.get('/api/players', (req, res) => {
  res.json({
    "message": "success",
    "data": players
  })
});

app.get('/api/players/:id/details', (req, res) => {
  const playersDetails = players.filter(player => player.id === req.params.id)
  res.json({
    "message": "success",
    "data": playersDetails
  })
});


app.get('/health-check', (req, res) => res.send('check'))

app.listen(port, () => console.log(`Server listening on port ${port}!`))
