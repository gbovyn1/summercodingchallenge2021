import * as React from 'react';
import { DataGrid } from '@material-ui/data-grid';
import Drawer from '@material-ui/core/Drawer';
import players from '../players';

const columns = [
    { field: 'player_name', headerName: 'Player name', width: 200 },
    { field: 'position', headerName: 'Position', width: 200 },
    { field: 'country', headerName: 'Country', width: 200 },
    {
        field: 'age',
        headerName: 'Age',
        type: 'number',
        width: 200,
    }
];

const rows = [
    ...players
];

export const PlayerTable = () => {

    const [drawerOpen, setDrawerOpen] = React.useState(false);
    const [selectedPlayer, setSelectedPlayer] = React.useState();

    const displayPlayerDetails = (player) => {
        setSelectedPlayer(player);
        
        setDrawerOpen(true);
    };

    return (
      <div style={{ height: 600, width: '100%' }}>
        <DataGrid
          rows={rows}
          columns={columns}
          pageSize={25}
          checkboxSelection
          onRowClick={(e) => displayPlayerDetails(e.row)}
        />

        <Drawer anchor={'bottom'} open={drawerOpen} onClose={() => setDrawerOpen(false)}>
          {selectedPlayer && <div>{selectedPlayer.player_name}</div>}
        </Drawer>
      </div>
    )
};