import './App.css';
import { PlayerTable } from './components/PlayerTable';

function App() {
  return (
    <div className="App">
       <PlayerTable />
    </div>
  );
}

export default App;
